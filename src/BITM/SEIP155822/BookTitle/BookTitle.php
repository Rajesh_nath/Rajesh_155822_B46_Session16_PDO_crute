<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/26/2017
 * Time: 7:25 PM
 */

namespace App\BookTitle;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class BookTitle extends DB
{
    private  $id;
    private $book_name;
    private $author_name;
    private $soft_deleted;

    public function setData($postData)
    {
        if (array_key_exists("id", $postData)) {
            $this->id = $postData["id"];
            echo $this->id;
        }

        if (array_key_exists("bookName", $postData)) {
            $this->book_name = $postData["bookName"];
            echo $this->book_name;
        }

        if (array_key_exists("authorName", $postData)) {
            $this->author_name = $postData["authorName"];
           echo $this->author_name;
        }

        if (array_key_exists("softDeleted", $postData)) {
           echo $this->soft_deleted = $postData["softDeleted"];
        }

    }

    public function store(){

        $bookName= $this->book_name;
        $authorName = $this->author_name;
        $dataArray = array($bookName,$authorName);
        $sql = "insert into book_title(book_name,author_name) VALUES(?,?)";
        //$this->DBH->exec($sql);
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($dataArray);
        if($result)
        {
            Message::message("Data hes been Inserted Successfully!!<br>");
        }
        else{
            Message::message("Failed! Data hasnot been inserted");
        }

        Utility::redirect('create.php');
    }
    }