<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/26/2017
 * Time: 9:06 PM
 */

namespace App\Message;


class Message
{
    public static function message($data=null){
        if(is_null($data)){
            return self::getMessage();
        }
        else{
            self::setMessage($data);
        }

    }

    public static function setMessage($data){
        $_SESSION['message']=$data;
    }

    public static function getMessage(){
        return $_SESSION['message'];
    }
}