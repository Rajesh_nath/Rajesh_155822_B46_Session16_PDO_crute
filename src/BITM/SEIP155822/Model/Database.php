<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/26/2017
 * Time: 6:45 PM
 */

namespace App\Model;
use PDO,PDOException;


class Database
{
    public $DBH;
    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        try{
            $this->DBH = new PDO('mysql:host=localhost; dbname=atomic_project_b46','root','');
           echo "congratulation! Db connected!";
        }
        catch(PDOException $error){
            echo "Error!: ".$error->getMessage()."<br/>";
            die();
        }
    }

}